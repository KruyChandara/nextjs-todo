import firestore from '../utils/db'
import { getDocs, collection, addDoc, deleteDoc, doc, updateDoc, onSnapshot } from 'firebase/firestore'

export const todoService = {
  get: async () => {
    const todoCol = collection(firestore, 'todo')
    const todoSnapshot = await getDocs(todoCol);
    const todoList = todoSnapshot.docs.map(doc => {
      return {...doc.data(), ref: doc.id}
    });
    return todoList;
  },
  post: async (payload) => {
    const todoCol = collection(firestore, 'todo')
    await addDoc(todoCol, payload);
  },
  update: async (payload, ref) =>{
    const docRef = doc(firestore, "todo", ref);
    await updateDoc(docRef, payload)
  },
  delete: async (ref) => {
    const docRef = doc(firestore, "todo", ref);
    await deleteDoc(docRef)
  },
  listenChange: (callback) => {
    const unsub = onSnapshot(collection(firestore, "todo"), callback);
    return unsub;
  }
}