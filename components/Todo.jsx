import React, { useRef, useEffect } from "react";
import styles from '../styles/Home.module.css'

const typeTimeOutDelay = 600;

export function Todo({ title, date, onClick, onUpdate }){
  const ref = useRef(null)
  useEffect(()=> {
    const ele = ref.current
    let timeOut = null;
    ele.addEventListener("keyup", function(e) {
      clearTimeout(timeOut)
      timeOut = setTimeout(()=>{
        const currentText = e.target.innerText;
        onUpdate(currentText)
        ele.blur();
      }, typeTimeOutDelay)
    }, false);
  }, [])

  return (
    <div className={styles.todo}>
      <div>
        <p ref={ref} contentEditable="true" suppressContentEditableWarning={true}>{title}</p>
        <p style={{fontSize: '12px', color: 'gray'}}>{date.toDate().toDateString()}</p>
      </div>
      <div>
        <button style={{marginRight: '10px'}} onClick={onClick} type="button">Delete</button>
      </div>
    </div>
  )
}