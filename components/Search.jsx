import React from 'react'

export function Search({ onChange }){
  let typeTimeOut = null;
  function onKeyUp(e){
    clearTimeout(typeTimeOut)
    typeTimeOut = setTimeout(() => {
      onChange(e.target.value)
    }, 600);
  }
  return <input type="text" placeholder='search...' onKeyUp={onKeyUp}/>
}